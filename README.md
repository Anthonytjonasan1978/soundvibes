# YOLO  (full stack React website)

## Demo
http://104.248.95.72/

## What is it
This is a small fun project. 
<br>It is a site where all the letters are connected to sound and show circles that fade away.

## Screenshot
![Screenshot](https://res.cloudinary.com/dkh74tuj6/image/upload/v1580742007/Screenshot_2020-02-03_at_15.57.01_b3cslf.png)

## Libraries used
<ul>
<li>Paper-full.js</li>
<li>Howler</li>
</ul>



## Installing
<ol>
<li>Clone this project</li>
<li>run **npm install** to install all packages needed</li>

</ol>


## Versions
1.0 Core features released 

# Developer
Tony Tjon a San (anthonytjonasan1978@gmail.com)
